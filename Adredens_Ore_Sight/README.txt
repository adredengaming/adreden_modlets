To use this mod:
1. Go to the /configs/blocks.xml
2. Open it
3. Remove the xml comment tags ( <!-- --> ) for the type of our your looking for.
4. Load the game and look for the specific ore. 

Note: It is best to use only one ore type at time. At the moment There are not different colours for the different ores yet.
Perhaps if there is enough support for this modlet I will use some time for makeing new shaders for the different ore types.

Video of modlet in use:
https://youtu.be/VkcPx7LZBZ8